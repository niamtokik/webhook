%%%-------------------------------------------------------------------
%%% @author Mathieu Kerjouan <contact@steepath.eu>
%%% @copyright 2020 (c) Mathieu Kerjouan
%%%
%%% @doc webhook main application
%%% @end
%%%-------------------------------------------------------------------
-module(webhook_app).
-behaviour(application).
-export([start/2, stop/1]).

%%--------------------------------------------------------------------
%% @doc start/2
%% @end
%%--------------------------------------------------------------------
-spec start(StartType, StartArgs) -> Return when
      StartType :: application:start_type(),
      StartArgs :: term(),
      Return :: pid().
start(_StartType, _StartArgs) ->
    webhook_sup:start_link().

%%--------------------------------------------------------------------
%% @doc stop/1
%% @end
%%--------------------------------------------------------------------
-spec stop(State) -> Return when
      State :: term(),
      Return :: ok.
stop(_State) ->
    ok.
