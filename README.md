# webhook

webhook is an application to get webhook and convert them in another
comprehensible format for different kind of external services. This is
a kind of translator between services.

## Usage

```sh
rebar3 compile
```

## Test

```sh
rebar3 ct
rebar3 eunit
```

# About

Made with <3 by Mathieu Kerjouan with [Erlang](erlang.org/) and
[rebar3](https://www.rebar3.org).
